﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using TestMobile.Models;

namespace TestMobile.testMobileDataSource
{
    
    class PostDataSource
    {

        public async void getPosts()
        {
            HttpClient client = new HttpClient();
            var responce = await client.GetStringAsync("https://jsonplaceholder.typicode.com/posts");
            var Posts = JsonConvert.DeserializeObject<List<Post>>(responce);
        }

        //public List<Post> Posts = new List<Post>()
        //{
        //    new Post()
        //    {
        //        postId=1,
        //        auther="kasun Madusanka",
        //        title = "FirstPost",
        //        details = "This is the first post of this application. nice to see you from this application and very good to see you as well. Hello there how are yu then."
        //    },
        //     new Post()
        //    {
        //        postId=2,
        //        auther = "chanaka madsanka",
        //        title = "SecondPost",
        //        details = "This is very short description and this will not going to go long."
        //    },
        //     new Post()
        //    {
        //        postId=3,
        //        auther="Sanjaya Madusanka",
        //        title = "THird Post",
        //        details = "This is the first post of this application. nice to see you from this application and very good to see you as well. Hello there how are yu then."
        //    },
        //     new Post()
        //    {
        //        postId=4,
        //        auther = "Hashan Gunawardane",
        //        title = "Fourth Post",
        //        details = "This is very short description and this will not going to go long."
        //    }
        //};
    }
}
