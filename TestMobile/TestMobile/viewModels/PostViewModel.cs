﻿using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using TestMobile.Models;
using System.Collections.ObjectModel;
using TestMobile.testMobileDataSource;


namespace TestMobile.viewModels
{
    class PostViewModel
    {

        private ObservableCollection<Post> posts;

        public ObservableCollection<Post> Posts
        {
            get { return posts; }
            set
            {
                posts = value;
            }
        }

        //public PostViewModel()
        //{
        //    Posts = new ObservableCollection<Post>();
        //    PostDataSource _contex = new PostDataSource();

        //    foreach(var post in _contex.Posts)
        //    {
        //        Posts.Add(post);
        //    }
        //}
    }
}
