﻿using System;
using System.Collections.Generic;
using System.Text;
using TestMobile.Models;
using TestMobile.testMobileDataSource;
using System.Collections.ObjectModel;

namespace TestMobile.viewModels
{
    class CommentViewModel
    {
        private ObservableCollection<Comment> comments;

        public ObservableCollection<Comment> Posts
        {
            get { return comments; }
            set
            {
                comments = value;
            }
        }

        //public CommentViewModel()
        //{
        //    Posts = new ObservableCollection<Comment>();
        //    CommentDataSource _contex = new CommentDataSource();

        //    foreach (var post in _contex.Posts)
        //    {
        //        Posts.Add(post);
        //    }
        //}
    }
}
