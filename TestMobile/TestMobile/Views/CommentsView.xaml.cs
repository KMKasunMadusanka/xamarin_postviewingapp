﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMobile.viewModels;
using Plugin.Messaging;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json;
using TestMobile.Models;

namespace TestMobile.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PostPage : ContentPage
	{
		public PostPage (int postId)
		{
			InitializeComponent ();
            this.InitializeComponent();
            this.BindingContext = this;
            this.IsBusy = false;

            getComments(postId);
            //BindingContext = new CommentViewModel();
        }

        public async void getComments(int id)
        {
            this.IsBusy = true;
            HttpClient client = new HttpClient();
            var responce = await client.GetStringAsync("https://jsonplaceholder.typicode.com/posts/"+id+"/comments");
            var Posts = JsonConvert.DeserializeObject<List<Comment>>(responce);
            commentView.ItemsSource = Posts;
            this.IsBusy = false;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (Comment)e.SelectedItem;

            var emailMessenger = CrossMessaging.Current.EmailMessenger;
            if (emailMessenger.CanSendEmail)
            {
                // Send simple e-mail to single receiver without attachments, bcc, cc etc.
                emailMessenger.SendEmail(item.email, "Regarding Post Comment", "Hi This is regarding your post on ABC App");

                // Alternatively use EmailBuilder fluent interface to construct more complex e-mail with multiple recipients, bcc, attachments etc.
                var email = new EmailMessageBuilder()
                  .To(item.email)
                  //.Cc("cc.plugins@xamarin.com")
                  //.Bcc(new[] { "bcc1.plugins@xamarin.com", "bcc2.plugins@xamarin.com" })
                  .Subject("Regarding Post Comment")
                  .Body("Hi This is regarding your post on ABC App")
                  .Build();

                emailMessenger.SendEmail(email);
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {

            Navigation.PushAsync(new UserPage());

        }
    }
}