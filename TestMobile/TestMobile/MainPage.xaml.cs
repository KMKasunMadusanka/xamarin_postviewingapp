﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using TestMobile.Views;
using TestMobile.viewModels;
using System.Net.Http;
using Newtonsoft.Json;
using TestMobile.Models;

namespace TestMobile
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            this.InitializeComponent();
            this.BindingContext = this;
            this.IsBusy = false;
            // BindingContext = new PostViewModel();
            getCustomers();
        }



        //private void Button_Clicked(object sender, EventArgs e)
        //{
        //    Navigation.PushAsync(new PostPage());
        //}
        public async void getCustomers()
        {
            this.IsBusy = true;
            HttpClient client = new HttpClient();
            var responce = await client.GetStringAsync("https://jsonplaceholder.typicode.com/posts");
            var Posts = JsonConvert.DeserializeObject<List<Post>>(responce);
            postListView.ItemsSource = Posts;
            this.IsBusy = false;
        }
        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (Post)e.SelectedItem;
            Navigation.PushAsync(new PostPage(item.id));
        }
    }
}
